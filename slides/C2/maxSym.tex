%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- MAXSYM SPACES -- %%%%%%%%%%%
\subsection{Espaços Maximalmente Simétricos}
{
\begin{fframe}
    {\Large \bfseries \subsecname}

    Espaços maximalmente simétricos são {\bfseries homogêneos e isotrópicos em todos os
    pontos}.

    \begin{itemize}
        \item<2-> Homogeneidade: Translações infinitesimais em relação à identidade em
                  $x_0$
                  \begin{align}
                      \tikzmarknode{KVcomp}
                      {
                          \txi[^{(\lambda)}_\mu] 
                      }(x_0, x_0) 
                      := \delta^\lambda_\mu
\iftoggle{tikz}{
                      \begin{tikzpicture}[remember picture, overlay]
                          \node<3>[expblock](KVcompblk) at (-6,0)
                                             {Um dos $N$ vetores};
                          \draw<3>[exparrow](KVcompblk) to[in=90,out=0] (KVcomp);
                      \end{tikzpicture}
}{}
                      \label{maxsym:homKillingVectors}
                      \,;
                  \end{align}
        \begin{itemize}
            \item<3-> $x_0$ arbitrário $\implies$ todo o espaço tem o mesmo aspecto.
        \end{itemize}

        \item<4-> Isotropia: Rotações infinitesimais que deixam um ponto $x_0$ fixo
            \begin{align}
                \begin{split}
                    \tikzmarknode{KVrot}
                    {
                        \txi[^{(\alpha\beta)}_\mu]
                    } (x,x_0) 
                    &:= - \txi[^{(\beta\alpha)}_\mu](x,x_0)
                    \,, \\
                    \txi[^{(\alpha\beta)}_\mu](x_0,x_0)
                    &:= 0
                    \,, \\
                    \txi[^{(\alpha\beta)}_{\lambda;\sigma}](x_0,x_0) 
                    &:= \bigg[ 
                             \dpar{ }{x^\sigma} \txi[^{(\alpha\beta)}_\lambda](x,x_0)
                        \bigg]_{x=x_0}
                      = \delta^\alpha_\lambda \delta^\beta_\sigma
                        - \delta^\alpha_\sigma \delta^\beta_\lambda
                        \..
                \end{split}
\iftoggle{tikz}{
                \begin{tikzpicture}[remember picture, overlay]
                    \node<5-6>[expblock](KVrotblk) at (0,2)
                                       {$\xi_\mu^{(\alpha\beta)}
                                         := \bigg[ \xi_\mu^{(\alpha)} \bigg]^{(\beta)}$
                                      };
                    \node<6>[expblock](KVrotblkblk) at (-2,4)
                    {
                        Um dos $\frac{1}{2}N(N-1)$ vetores
                    };
                    \draw<5-6>[exparrow](KVrotblk) to[in=90,out=-90] (KVrot);
                    \draw<6>[exparrow](KVrotblkblk) to[in=90,out=-90] (KVrotblk);
                \end{tikzpicture}
}{}
                \label{maxsym:isoKillingVectors}
            \end{align}
    \end{itemize}
    
    \visible<7->{
        Ambos os casos correspondem a vetores \alert{linearmente independentes}!
    }
\end{fframe}

\begin{fframe}
    \begin{itemize}
    \item Isotropia em todo o espaço:
    \begin{itemize}
        \item<2-> Supondo um vetor de Killing isotrópico em uma vizinhança $dx_0$ 
                  arbitrária
                  \begin{align*}
                      \xi^{(\alpha\beta)}_\mu(x, x_0 + dx_0)
                      \,;
                  \end{align*}
        \item<3-> Expandindo em torno de $x_0$ e contraindo
                  \begin{align*}
                      \dpar{ }{x_0^\alpha}  \txi[^{(\alpha\beta)}_\mu](x,x_0)\bigg|_{x_0}
                      = - (N-1)\tikzmarknode{homger}{\delta^\beta_\mu}
                      \,;
\iftoggle{tikz}{
                      \begin{tikzpicture}[remember picture,overlay]
                          \node<4->[expblock](homgerblk) at (2,2)
                          {
                              Translação infinitesimal
                          };
                          \draw<4->[exparrow](homgerblk) to[out=-90,in=90] (homger);
                      \end{tikzpicture}
}{}
                  \end{align*}
        \item<5-> Se $\xi_\mu(x = x_0) = a_\mu$, podemos construir
                  \begin{align}
                      \txi[_\mu](x) = \frac{a_\beta}{N-1} 
                                      \dpar{ }{x_0^\alpha} 
                                      \txi[^{(\alpha\beta)}_\mu](x,x_0)
                  \label{maxsym:isoHomKillingVectors}
                  \..
                  \end{align}

    \end{itemize}
    \end{itemize}

    \visible<7->{
        Total de $\frac{1}{2}N(N+1)$ vetores de Killing%
    }%
    \visible<8->{% 
        , logo espaços maximalmente simetricos são isotrópicos e homogêneos em todo o
        espaço!
    }
\end{fframe}

\begin{fframe}
    Pela arbitrariedade de \tikzmarknode[ref]{KVx0}{\ref{maxsym:taylorExpansion}},
    escolhemos
\iftoggle{tikz}{
    \begin{tikzpicture}[remember picture, overlay]
        \node<2>[expblock](KVx0ref) at (3,1)
                          {
                           $\txi[_\mu](x, x_0) =
                            \tA[_\mu^\lambda](x,x_0) 
                            \tikzmarknode{kv}{\txi[_\lambda] (x_0)} +
                            \tB[_\mu^{\lambda\sigma}](x,x_0) 
                            \txi[_{\lambda;\sigma}] (x_0)$
                          };
        \draw<2>[exparrow](KVx0ref) to[out=180,in=30] (KVx0);
    \end{tikzpicture}
}{}

    \visible<3->{
    \vspace{-.3in}
    \begin{align}
        \begin{cases}
            \txi[_\mu](x)       &= 0 \,,\\
            \txi[_{\mu;\nu}](x) &\ne 0  \,,
        \end{cases}
        \label{maxsym:arbitraryIsoVectors}
    \end{align}
    }

    \visible<4->{
    de modo que \tikzmarknode[ref]{intCond}{\ref{maxsym:integrabilityConds}} resulta, após
    uma série de contrações,
    }

\iftoggle{tikz}{
    \begin{tikzpicture}[remember picture, overlay]
        \node<5>[expblock](intCondref) at (6,2)
                          {
                          $\bigg(
                            - \tR[^\lambda_{\rho\sigma\nu}] \delta^\kappa_\mu
                            + \tR[^\lambda_{\mu\sigma\nu}]  \delta^\kappa_\rho
                            - \tR[^\lambda_{\nu\mu\rho}]    \delta^\kappa_\sigma
                            + \tR[^\lambda_{\sigma\mu\rho}] \delta^\kappa_\nu
                            \bigg)\ \txi[_{\lambda;\kappa}]
                            = 
                            ( \tR[^\lambda_{\nu\mu\rho;\sigma}] 
                            - \tR[^\lambda_{\sigma\mu\rho;\nu}] )\ \txi[_\lambda]$
                          };
        \draw<5>[exparrow](intCondref) to[out=-90,in=30] (intCond);
    \end{tikzpicture}
}{}

    \visible<6->{
    \vspace{-.3in}
    \begin{align}
        \begin{cases}
            \tR[_{\lambda\rho\sigma\nu}] 
            &= \tikzmarknode{K1}{K} \bigg(
                    \tg[_\nu_\lambda] \tg[_\rho_\sigma]
                    - \tg[_\nu_\rho] \tg[_\lambda_\sigma]
                 \bigg) \\
            \tR[_{\mu\nu}] 
            &= (N-1) \tikzmarknode{K2}{K} \tg[_\mu_\nu]
        \end{cases}
        \label{maxsym:ConstantCurvatureMaxSymTensors}
        \..
\iftoggle{tikz}{
        \begin{tikzpicture}[remember picture,overlay]
            \node<7->[expblock](Kblk) at (-2,-1.5) {Constante de curvatura};
            \draw<7->[exparrow](Kblk) to[out=90,in=-120] (K1);
            \draw<7->[exparrow](Kblk) to[out=90,in=-45] (K2);
        \end{tikzpicture}
}{}
    \end{align}
    }

    \visible<8->{
    $\rightarrow$ Únicos!
    }
\end{fframe}

\begin{fframe}
    Construção do elemento de linha maximalmente simétrico:

    \begin{itemize}
        \item<2-> Imersão num $(N+1)-$espaço numa $N-$esfera 
        \begin{align}
            ds^2 
            = \tikzmarknode{realMetric}{h_{\mu\nu}} dx^\mu dx^\nu + K^{-1} dz^2
            \,, \qquad \mu,\nu = 0, \ldots, N-1
            \,,
            \label{maxsym:embeddingIntervalWithZ}
\iftoggle{tikz}{
            \begin{tikzpicture}[remember picture,overlay]
                \node<3>[expblock](realMetricblk) at (-1,1) {Métrica do espaço real};
                \draw<3>[exparrow](realMetricblk) to[out=180,in=90] (realMetric);
            \end{tikzpicture}
}{}
        \end{align}

        \visible<4->{
        sujeito à condição de vínculo
        \begin{align}
            K h_{\mu\nu} x^\mu x^\nu + z^2 = 1
            \label{maxsym:parametricN+1equation}
            \,,
        \end{align}
        }

        \visible<5->{
        implica
        \begin{align}
            ds^2 
            &= \tikzmarknode[highlight]{embMetric}{
            \bigg[
                h_{\mu\nu} + \frac{K}{1 - K h_{\alpha\beta} x^\alpha x^\beta}
                      h_{\mu\sigma} x^\sigma h_{\nu\rho} x^\rho
            \bigg]} dx^\mu dx^\nu
            \label{maxsym:embeddingInterval}
            \..
\iftoggle{tikz}{
            \begin{tikzpicture}[remember picture,overlay]
                \node<6->[expblock](embMetricblk) at (-5,-1.5){$g_{\mu\nu}$};
                \draw<6->[exparrow](embMetricblk) to[in=-90,out=90] (embMetric);
            \end{tikzpicture}
}{}
        \end{align}
        }
    \end{itemize}

\end{fframe}

\begin{fframe}
    Imersão na $N-$esfera deve ser invariante sob rotações de $\mathcal{SO}(N+1)$:

    \visible<2->{
    \begin{align}
        \begin{cases}
            \ x^\mu 
            &\rightarrow x'^\mu
            =
            \tensor{R}{^\mu_\nu} x^\nu + \tensor{R}{^\mu_z} z
                                                                                          \\
            \ z
            &\rightarrow z'
            =
            \tensor{R}{^z_\nu} x^\nu + \tensor{R}{^z_z} z
        \end{cases}
        \label{maxsym:rigidRotationSymmetry}
        \..
    \end{align}
    }

    \visible<3->{
        Impondo invariância do elemento de linha :
        \tikzmarknode[ref]{ds2}{\ref{maxsym:embeddingIntervalWithZ}}
    }
\iftoggle{tikz}{
    \begin{tikzpicture}[remember picture,overlay]
        \node<4>[expblock](ds2blk) at (2,1) 
                          {
                            $ds^2 = h_{\mu\nu} dx^\mu dx^\nu + K^{-1} dz^2$
                          };
        \draw<4>[exparrow](ds2blk) to[out=-90,in=30] (ds2);
    \end{tikzpicture}
}{}

    \visible<5->{
    \begin{align}
        \begin{split}
            h_{\alpha\beta} \tensor{R}{^\alpha_\mu}
                            \tensor{R}{^\beta_\nu}
          + K^{-1} \tensor{R}{^z_\mu}
                   \tensor{R}{^z_\nu}
          &= h_{\mu\nu}
          \,, \\
              h_{\alpha\beta} \tensor{R}{^\alpha_\sigma}
                          \tensor{R}{^\beta_z}
            + K^{-1} \tensor{R}{^z_\sigma}
                     \tensor{R}{^z_z}
          &= 0
          \,, \\
             h_{\alpha\beta} \tensor{R}{^\alpha_z}
                            \tensor{R}{^\beta_z}
          + K^{-1} (\tensor{R}{^z_z})^2
          &= K^{-1}
          \..
        \end{split}
        \label{maxsym:rotationSymmetries}
    \end{align}

    }
\end{fframe}

\begin{fframe}
    Sob transformações infinitesimais $x \rightarrow x' + \varepsilon \xi$, obtém-se

    \visible<2->{
    \begin{align}
        \txi[^\mu_{iso}](x) 
        &= \tikzmarknode{infrot}{\tOmega[^\mu_\nu]} 
           x^\nu
           \label{maxsym:embeddingIsotropicSymmetryKilling} 
           \,,\\
        \txi[^\mu_{homo}](x)
        &= \tikzmarknode{inftrans}{\alpha^\mu}
           (1 - K h_{\sigma\rho} x^\sigma x^\rho)^\frac{1}{2}
           \label{maxsym:embeddingHomogeneitySymmetryKilling}
           \..
\iftoggle{tikz}{
        \begin{tikzpicture}[remember picture,overlay]
            \node<3-4>[brkblock](infrotblk) at (1,2)
                                {
                                    Parâmetro infinitesimal de rotações\qquad\qquad
                                    $\Omega_{\mu\nu} = - \Omega_{\nu\mu}$
                                };
            \node<4>[brkblock](inftransblk) at (-6,1)
                              {
                                  Parâmetro infinitesimal de quasi-translação
                              };
            \draw<3-4>[exparrow](infrotblk) to[out=-90,in=30] (infrot);
            \draw<4>[exparrow](inftransblk) to[out=-90,in=-120] (inftrans);
        \end{tikzpicture}
}{}
    \end{align}
    }

    \begin{itemize}
        \item<5-> Total de $\frac{1}{2}N(N+1)$ vetores $\implies$ maximalmente simétrico!
    \end{itemize}
\end{fframe}

\begin{fframe}
    Logo, 
    \begin{align*}
        g_{\mu\nu} =
        h_{\mu\nu} + \frac{K}{1 - K h_{\alpha\beta} x^\alpha x^\beta}
              h_{\mu\sigma} x^\sigma h_{\nu\rho} x^\rho
    \end{align*}
    é uma métrica para espaços maximalmente simétricos!

    \vspace{1cm}

    \visible<2->{
        Impondo
        \begin{align}
            h_{\mu\nu} \equiv 
            \begin{cases}
            |K|^{-1} \delta_{\mu\nu}
            &, K \ne 0
            \\
            \delta_{\mu\nu}
            &, K = 0
            \end{cases}
            \label{maxsym:embeddingEuclidMetric}
            \,,
        \end{align}%
    }%
    \visible<3->{%
        resulta em
        \begin{align}
            ds^2 = d\bs{x}^2 
                 + \tikzmarknode{k}{k} \frac{(\bs{x} \cdot d\bs{x})^2}{1 - k \bs{x}^2}
            \label{maxsym:embeddingEuclidIntervalsWithk}
            \..
\iftoggle{tikz}{
            \begin{tikzpicture}[remember picture,overlay]
                \node<4->[expblock](kblk) at (2, 1) 
                                   {Parâmetro de curvatura $k=\{0, \pm1\}$};
                \draw<4->[exparrow](kblk) to [in=90, out=180] (k);
            \end{tikzpicture}
}{}
        \end{align}
    }
\end{fframe}

\begin{fframe}
    {\bfseries Teorema de subdivisão:}
    \begin{columns}
    \column{.7\textwidth}
    \begin{itemize}
        \item<2-> Suponha que um espaço $\mathcal{M}(N)$ possa ser quebrado num subespaço
                  admitindo simetria máxima $\mathcal{S}(M)$ e no seu complementar 
                  $\mathcal{\bar{S}}(N-M)$ não maximalmente simétrico;
        \item<3-> Discriminamos as coordenadas $u$ e $v$, onde
                  \begin{align}
                      \begin{split}
                          u^i &\rightarrow u'^i = u^i + \varepsilon \xi^i(u;v) \,, \\
                          v^a &\rightarrow v'^a = v^a \..
                      \end{split}
                      \label{maxsym:subspaceInvariantTransformations}
                  \end{align}
        Então o tensor métrico é descrito por
        \begin{align}
            ds^2 = g_{\mu\nu} dx^\mu dx^\nu
            = \tikzmarknode{nonsymg}{ g_{ab}(v) } dv^a dv^b + 
              \tikzmarknode{paramf} { f(v) }
              \tikzmarknode{maxsymg}{ \tilde{g}_{ij}(u) } du^i du^j
            \label{maxsym:subspaceMetricTheorem}
            \..
\iftoggle{tikz}{
            \begin{tikzpicture}[remember picture,overlay]
                \node<4>[expblock](nonsymgblk) at (-6,-1)
                                  {
                                      Métrica de $\mathcal{\bar{S}}$
                                  };
                \node<5>[expblock](paramfblk) at (-3,1)
                                  {
                                      Peso conforme paramétrico ($v$)
                                  };
                                  \node<6>[expblock](maxsymgblk) at (0,-1.2)
                                  {
                                      Métrica maximalmente simétrica de $\mathcal{S}$
                                  };
                \draw<4>[exparrow](nonsymgblk) to[out=90,in=-90] (nonsymg);
                \draw<5>[exparrow](paramfblk) to[out=-90,in=120] (paramf);
                \draw<6>[exparrow](maxsymgblk) to[out=90,in=-90] (maxsymg);
            \end{tikzpicture}
}{}
        \end{align}

    \end{itemize}


    \column{.3\textwidth}
    \visible<2->{
    \begin{figure}[h!]
        \centering
        \includegraphics[width=\textwidth] {res/MaxSimSubspace.pdf}
        \label{fig:subdivisionTheorem}
    \end{figure}
    }
    \end{columns}
\end{fframe}

\iffalse
\begin{fframe}
    Algumas passagens da prova:

%\iftoggle{tikz}{
    \begin{figure}[h!]
        \centering
    \begin{tikzpicture}
        \node[visible on=<2->,expblock](metric) at (0,0) {$g_{\mu\nu}$};
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        \node[visible on=<3->,expblock](metricij) at (-2,-2) {$g_{ij}$};
        \node[visible on=<3->,expblock](metricia) at (0,-2) {$g_{ia}$};
        \node[visible on=<3->,expblock](metricab) at (2,-2) {$g_{ab}$};
        \draw[visible on=<3->,exparrow](metric) to[out=-90,in=90] (metricij);
        \draw[visible on=<3->,exparrow](metric) to[out=-90,in=90] (metricia);
        \draw[visible on=<3->,exparrow](metric) to[out=-90,in=90] (metricab);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        \node[visible on=<4->,expblock](metricijlie) at (-2,-3) {$\mathcal{L}[g_{ij}]=0$};
        \node[visible on=<4->,expblock](metricialie) at (0,-3) {$\mathcal{L}[g_{ia}]=0$};
        \node[visible on=<4->,expblock](metricablie) at (2,-3) {$\mathcal{L}[g_{ab}]=0$};
        \draw[visible on=<4->,exparrow](metricij) to[out=-90,in=90] (metricijlie);
        \draw[visible on=<4->,exparrow](metricia) to[out=-90,in=90] (metricialie);
        \draw[visible on=<4->,exparrow](metricab) to[out=-90,in=90] (metricablie);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \end{tikzpicture}
    \end{figure}
%}{}

    \begin{itemize}
        \item<5-> Encontra-se uma transformação de coordenadas que desacople $u$ e $v$;
        \item<6-> Covariante $\rightarrow$ vale para todo referencial;
        \item<7-> Chega no resultado
            \tikzmarknode[ref]{result}{\ref{maxsym:subspaceMetricTheorem}}.
\iftoggle{tikz}{
            \begin{tikzpicture}[remember picture,overlay]
                \node<8>[expblock](resultblk) at (1.5,1)
                                  {
                                    $ds^2 = g_{ab}(v) dv^a dv^b + 
                                            f(v) \tilde{g}_{ij}(u) du^i du^j$
                                  };
                                  \draw<8>[exparrow](resultblk) to[out=-90,in=30] (result);
            \end{tikzpicture}
}{}
    \end{itemize}
\end{fframe}
\fi

\begin{fframe}
    Já que $\mathcal{S}$ é maximalmente simétrico, podemos escrever o elemento de linha
    como, usando \tikzmarknode[ref]{MSmetric}{\ref{maxsym:embeddingEuclidIntervalsWithk}},

\iftoggle{tikz}{
    \begin{tikzpicture}[remember picture,overlay]
        \node<1>[expblock](MSmetricblk) at (4,0)
                          {
                              $ds^2 = d\bs{x}^2 
                                    + k \frac{(\bs{x} \cdot d\bs{x})^2}{1 - k \bs{x}^2}$
                          };
        \draw<1>[exparrow](MSmetricblk) to[out=90,in=-30] (MSmetric);
    \end{tikzpicture}
}{}

    \visible<2->{
    \begin{align}
        ds^2 = g_{ab}(v) dv^a dv^b
        + f(v) \bigg[
            d\boldsymbol{u}^2 + k \frac{(\boldsymbol{u} \cdot d\boldsymbol{u})^2}
                                       {1 - k \boldsymbol{u}^2}
        \bigg]
        \label{maxsym:subspaceIntervalEuclidSpace}
        \..
    \end{align}
    }

\end{fframe}

\begin{fframe}
    {\bfseries Exemplos:}

    \visible<2->{
    \begin{exampleblock}{Espaço-tempo maximalmente simétrico}
        De \tikzmarknode[ref]{embbed}{\ref{maxsym:embeddingInterval}}, com 
        $h_{\mu\nu} \equiv \eta_{\mu\nu}$:
\iftoggle{tikz}{
        \begin{tikzpicture}[remember picture,overlay]
            \node<2>[expblock](embbedblk) at (2,2)
                              {
                                  $ds^2 = \bigg[
                                       h_{\mu\nu} + 
                                       \frac{K}{1 - K h_{\alpha\beta} x^\alpha x^\beta}
                                       h_{\mu\sigma} x^\sigma h_{\nu\rho} x^\rho
                                   \bigg] dx^\mu dx^\nu$
                              };
            \draw<2>[exparrow](embbedblk) to[in=30,out=-90] (embbed);
        \end{tikzpicture}
}{}

        \visible<3->{
        \begin{align}
            ds^2
            &=
            dt^2 - d\boldsymbol{x}^2
            + \frac{K}{1 - K h_{\alpha\beta} x^\alpha x^\beta}
            \bigg(
                t dt - \boldsymbol{x} \cdot d\boldsymbol{x}
            \bigg)^2
            \label{maxsym:embeddingMinkowskiInterval}
            \,;
        \end{align}
        }

        \begin{itemize}
            \item<4-> Mudança de coordenadas:
        \end{itemize}

        \visible<4->{
        \begin{align}
            ds^2
            &=
            (1 - K \boldsymbol{x''}^2) dt''^2
            - d\boldsymbol{x''}^2
            - \frac{K}{1 - K \boldsymbol{x''}^2}
                \big( \boldsymbol{x''} \cdot d\boldsymbol{x''} \big)^2
            \label{maxsym:embeddingMinkowskiDeSitterInterval}
            \..
        \end{align}
        }

        \begin{itemize}
            \item<5-> Universo \alert{de Sitter} ($K>0$) e \alert{Anti-de Sitter} ($K<0$) 
                      em um sistema de coordenadas hiperbólicas (ou estáticas)!
        \end{itemize}

    \end{exampleblock}
    }
\end{fframe}

\begin{fframe}
    {\bfseries Exemplos:}

    \visible<2->{
    \begin{exampleblock}{Espaço-tempo com setor espacial maximalmente simétrico}
        Usando \tikzmarknode[ref]{subdiv}{\ref{maxsym:subspaceIntervalEuclidSpace}},
        identificando
\iftoggle{tikz}{
        \begin{tikzpicture}[remember picture,overlay]
            \node<2>[expblock](subdivblk) at (2,2)
                              {
                                  $ds^2 = g_{ab}(v) dv^a dv^b
                                        + f(v) \bigg[
                                          d\boldsymbol{u}^2 
                                        + k \frac{(\boldsymbol{u} \cdot d\boldsymbol{u})^2}
                                                 {1 - k \boldsymbol{u}^2}
                                        \bigg]$
                              };
            \draw<2>[exparrow](subdivblk) to[in=30,out=-90] (subdiv);
        \end{tikzpicture}
}{}

        \visible<3->{
            \begin{align*}
            \begin{cases}
                v &= t \\
                u &= \{r, \theta, \phi\} = 
                \begin{cases}
                    u^1 = r \sin\theta \cos\phi \\
                    u^2 = r \sin\theta \sin\phi \\
                    u^3 = r \cos\theta
                \end{cases}
            \end{cases}
            \,,
            \end{align*}
        }

        \visible<4->{
            chega-se a
            \begin{align}
                ds^2
                &= 
                dt^2 - \tikzmarknode{scale}{S^2(t)} \bigg[
                    \frac{dr^2}{1-kr^2} + r^2 (d\theta^2 + \sin^2\theta d\phi^2)
                \bigg]
                \label{maxsym:subspaceHomST=Friedmann}
                \ \,, \quad k = \{0, \pm1\}
                \..
\iftoggle{tikz}{
                \begin{tikzpicture}[remember picture,overlay]
                    \node<5>[expblock](scaleblk) at (-2,1)
                                      { Fator de escala };
                    \draw<5>[exparrow](scaleblk) to[out=-90,in=30] (scale);
                \end{tikzpicture}
}{}
            \end{align}
        }

        \begin{itemize}
            \item<6-> Universo de Friedmann-Lemaître-Robertson-Walker (\FLRW)!
            \begin{itemize}
                \item<7-> Modelo padrão da cosmologia.
            \end{itemize}
        \end{itemize}
    \end{exampleblock}
    }

\end{fframe}

\begin{fframe}
    {\bfseries Algumas propriedades dos modelos \FLRW:}
    \begin{itemize}
        \item<2-> Três setores de curvatura:
        \begin{itemize}
            \item<2-> Setor plano $(k=0)$: Expansão indefinida;
            \item<2-> Setor aberto/hiperbólico $(k=-1)$: Expansão indefinida;
            \item<2-> Setor fechado/esférico $(k=+1)$: Idade limite (\alert{Big-crunch});
        \end{itemize}
        \item<3-> Aproximação de fluido em equilíbro
            \begin{align*}
                T^{\mu\nu} = (\varepsilon + p) u^\mu u^\nu - p g^{\mu\nu}
                \,;
            \end{align*}
        \item<4-> Fluido perfeito em equilíbro à pressão constante
            \begin{align*}
                \tT[^0_0] &= \varepsilon \,, \\
                \tT[^1_1] =
                \tT[^2_2] =
                \tT[^3_3] &= \text{const} = -p \,, \\
                \tT[^\mu_\nu] &= 0 
                \ \,, \quad \mu \ne \nu
                \,,
            \end{align*}

        \item<5-> Postulado de Weyl: $u^\mu = (1, \bs{0})$
        \begin{itemize}
            \item<6-> \alert{Aproximações válidas apenas quando energia desacopla da
                             matéria!}
            \item<6-> $\frac{S_0}{S} \lesssim 10^3$.
        \end{itemize}
        \item<7-> Recupera a lei de Hubble.
    \end{itemize}
\end{fframe}

}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- ------------- -- %%%%%%%%%%%
