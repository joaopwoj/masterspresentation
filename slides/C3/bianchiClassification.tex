%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- BIANCHI CLASSIFICATION -- %%%%%%%%%%%
\subsection{Classificação de Bianchi}
{
\begin{fframe}
    {\Large \bfseries \subsecname}

    Classificaremos algumas propriedades algébricas, físicas e topológicas das
    diferentes álgebras de Bianchi $\frak{b}_i$

    \begin{itemize}
        \item<2-> Conceitos adicionais de teoria de grupos:
        \begin{itemize}
            \item<3-> {\bfseries Álgebra derivada:} 
                      $\frak{g}' := [\frak{g},\frak{g}]$;
            \item<3-> {\bfseries Solubilidade:} $r$ tal que 
                      $\frak{g}^{(r)} = [\frak{g}^{(r-1)}, \frak{g}^{(r-1)}]$;
            \item<3-> {\bfseries Nilpotência:} $r$ tal que 
                      $\frak{g}^{r} = [\frak{g}, \frak{g}^{r-1}]$, 
                      onde $\frak{g}^1 := \frak{g}'$;
            \item<3-> {\bfseries Centro:}
                      $ Z(\frak{g}) := 
                      \{ X \in \frak{g} : [X, Y] = 0 \ 
                         \,,\quad \forall Y \in \frak{g} \}$;
            \item<3-> {\bfseries Radical:}
                      Maior subalgebra invariante (ideal) que seja solúvel.
        \end{itemize}
        \item<4-> Resultados físicos:
        \begin{itemize}
            \item<4-> Tríades%
                      \visible<4->{\footfullcite{ART:Taub2004GReGr..36.2699T}};
            \item<4-> Métrica;
            \item<4-> Propriedades diversas.
        \end{itemize}
    \end{itemize}
\end{fframe}

\begin{fframe}
    \begin{itemize}
        \item<1-> Topologias de Thurston%
                  \footfullcite{ART:Luminet1995PhR...254..135L}:
        \begin{itemize}
            \item<2-> Mínimas topologias.
        \end{itemize}

        \visible<2->{
        \begin{table}[h!]
        \centering
        \begin{tabular}{ccc}
             & $\mathcal{M}$                     & Obs                   \\ \hline
        i.   & $\mathds{E}^3$                    & Geometria Euclidiana  \\
        ii.  & $\mathds{S}^3$                    & Geometria Esférica    \\
        iii. & $Nil$                             & Grupo de Heisenberg   \\
        iv.  & $Sol$                             & Grupo Solúvel         \\
        v.   & $\mathds{H}^3$                    & Geometria Hiperbólica \\
        vi.  & $\widetilde{SL(2,\mathds{R})}$    & Grupo Linear Especial 
                                                   em duas dimensões%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                           \footnotemark \\
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        vii.  & $\mathds{H}^2 \times \mathds{E}$ & --                    \\
        viii. & $\mathds{S}^2 \times \mathds{E}$ & --                    \\ \hline
        \end{tabular}
        \label{tab:hom:thurston}
        \end{table}
        \footnotetext{Espaço de recobrimento universal do espaço tangente de
                      $\mathds{H}^2$.}
        }

    \end{itemize}

\end{fframe}

\begin{fframe}
    A classificação será feita em base da dimensão da álgebra derivada%
    \footfullcite{TH:Fowler2014} 
    $\frak{b}'$;

    \begin{itemize}
        \item<2-> Separados em grupos:
        \begin{itemize}
            \item<2-> dim $\frak{b}' = 0$: Tipo I;
            \item<2-> dim $\frak{b}' = 1$: Tipos II e III;
            \item<2-> dim $\frak{b}' = 2$: Tipos IV--VII;
            \item<2-> dim $\frak{b}' = 3$: Tipos VIII e IX.
        \end{itemize}
        \item<3-> Classificação mais moderna comparado com o trabalho de Bianchi%
                  \visible<3->{\footfullcite{ART:Bianchi2001GReGr..33.2171B}};

        \item<4-> Relembrando \tikzmarknode[ref]{bia}{\ref{hom:homoBianchiComutators}}

        \begin{align*}
            \begin{split}
            [ \tX[_1], \tX[_2] ]
            &= - a \tX[_2] + \tn[^{(3)}] \tX[_3]
            \,, \\
            [ \tX[_2], \tX[_3] ]
            &= \tn[^{(1)}] \tX[_1]
            \,, \\
            [ \tX[_3], \tX[_1] ]
            &= \tn[^{(2)}] \tX[_2] + a \tX[_3]
            \..
            \end{split}
        \end{align*}

        \item<5-> Exemplificaremos dois casos.
        
    \end{itemize}
\end{fframe}

\renewcommand\subsecname{Classificação -- $\frak{b}_{I}$}
{{{
\begin{fframe}
    Álgebra:
     \begin{align}
        [X_1, X_2] = 0 
        \,,
        \qquad
        [X_2, X_3] = 0
        \,,
        \qquad
        [X_3, X_1] = 0
        \..
        \label{bia:biaIAlgebra}
    \end{align}

    \begin{itemize}
        \visible<2->{
        \item {\bfseries Propriedades algébricas:}
        \begin{itemize}
            \item $\dim\ \frak{g}' = 0$.
            \item Abeliana: 
\iftoggle{tikz}{
            \begin{tikzpicture} [remember picture,overlay]
                \node[expblock] at (5,0)
                {$ X_i X_j = X_j X_i \ \,; \quad \forall X_i \in \frak{g}$;};
            \end{tikzpicture}
}{}
            \item Única a menos de isomofismos;
            \item Solúvel e nilpotente,
                \begin{align*}
                    R(\frak{g}) = Z(\frak{g}) = \frak{g}
                    \..
                \end{align*}
            \item Thurston type: $\mathds{E}^3$.
        \end{itemize}
        }

        \visible<3->{
        \item {\bfseries Propriedades físicas:}
        \begin{itemize}
            \item Métrica
                \begin{align*}
                    ds^2 = d\tau^2 - A^2(\tau) dx^2 - B^2(\tau) dy^2 - C^2(\tau) dz^2
                    \..
                \end{align*}
            \item Euclidiana;
            \item Não necessariamente isotrópica;
            \item Contém o setor plano do universo \FLRW\ ($k=0$)
            \item Tríades:
            \begin{align*}
            [\tetD{a}{i}] = \left(\begin{matrix}
                    1 & 0 & 0 \\
                    0 & 1 & 0 \\
                    0 & 0 & 1
               \end{matrix}\right)
               \,,
               \qquad
            [\tetU{a}{i}] = \left(\begin{matrix}
                    1 & 0 & 0 \\
                    0 & 1 & 0 \\
                    0 & 0 & 1
               \end{matrix}\right)
               \..
            \end{align*}

        \end{itemize}
        }
    \end{itemize}
\end{fframe}
}}}

\iffalse
\renewcommand\subsecname{Classificação -- $\frak{b}_{II}$}
{{{
\begin{fframe}
    Álgebra:
    \begin{align}
        [X_1, X_2] = 0
        \,,
        \qquad
        [X_2, X_3] = X_1
        \,,
        \qquad
        [X_3, X_1] = 0
        \..
        \label{bia:biaIIAlgebra}
    \end{align}

    \begin{itemize}
        \visible<2->{
        \item {\bfseries Propriedades algébricas:}
        \begin{itemize}
            \item $\dim\ \frak{b}' = 1$;
            \item $\frak{b}' = X_3$;
            \item Álgebra derivada: $\frak{b}' \subseteq Z(\frak{b})$;
            \item Não abeliana;
            \item Nilpotente;
            \item Solúvel;
            \item Radical: $R(\frak{b}) = \frak{b}$;
            \item $\frak{b} \approx \frak{h}$ -- Isomorfa à álgebra de Heisenber;
            \item Thurston type: $Nil$.
        \end{itemize}
        }

        \visible<3->{
        \item {\bfseries Propriedades físicas:}
        \begin{itemize}
            \item Métrica
                \begin{align*}
                    ds^2 = d\tau^2 - R^2(\tau) dx^2 
                                   - S^2(\tau) dy^2 - 2 S^2(\tau) x dy dz
                                   - \bigg(S^2(\tau) x^2 - R^2(\tau)\bigg) dz^2
                                   \,;
                \end{align*}

            \item Tríades:
            \begin{align*}
            [\tetD{a}{i}] = \left(\begin{matrix}
                    0 & 0 & 1   \\
                    1 & 0 & x^3 \\
                    0 & 1 & 0
               \end{matrix}\right)
               \,,
               \qquad
            [\tetU{a}{i}] = \left(\begin{matrix}
                    -x^3 & 1 & 0 \\
                    0    & 0 & 1 \\
                    1    & 0 & 0
               \end{matrix}\right)
               \..
            \end{align*}
        \end{itemize}
        }
    \end{itemize}
\end{fframe}

}}}

\renewcommand\subsecname{Classificação -- $\frak{b}_{III}$ $(a=1)$}
{{{
\begin{fframe}
    Álgebra:
    \begin{align}
        [Y_1, Y_2] = Y_1
        \,,
        \qquad
        [Y_3, Y_1] = 0
        \,,
        \qquad
        [Y_2, Y_3] = 0
        \..
        \label{bia:biaIIIAlgebra}
    \end{align}

    \begin{itemize}
        \visible<2->{
        \item {\bfseries Propriedades algébricas:}
        \begin{itemize}
            \item $\dim\ \frak{b}' = 1$;
            \item $\frak{b}' = Y_1$;
            \item Álgebra derivada: $\frak{b}' \nsubseteq Z(\frak{b})$;
            \item Não abeliana;
            \item Centro $Z(\frak{b}) = Y_3$ tem dim $= 1$;
            \item Não nilpotente;
            \item Solúvel;
            \item Radical: $R(\frak{b}) = \frak{b}$;
            \item Thurnston type: $\mathds{H}^2 \times \mathds{E}$.
        \end{itemize}
        }

        \visible<3->{
        \item {\bfseries Propriedades físicas:}
        \begin{itemize}
            \item Métrica
                \begin{align*}
                    ds^2 = d\tau^2 - A^2(\tau) dx^2 
                                   - B^2(\tau) e^{-2x} dy^2 - C^2(\tau) dz^2
                                   \,;
                \end{align*}
            \item Petrov tipo $D$;

            \item Tríades:
            \begin{align*}
            [\tetD{a}{i}] = \left(\begin{matrix}
                    0 & 0 & 1   \\
                    1 & 0 & x^2 \\
                    0 & 1 & 0
               \end{matrix}\right)
               \,,
               \qquad
            [\tetU{a}{i}] = \left(\begin{matrix}
                    -x^2 & 1 & 0 \\
                    0    & 0 & 1 \\
                    1    & 0 & 0 
               \end{matrix}\right)
               \..
            \end{align*}

        \end{itemize}
        }
    \end{itemize}
\end{fframe}
}}}

\renewcommand\subsecname{Classificação -- $\frak{b}_{IV}$}
{{{
\begin{fframe}
    Álgebra:
    \begin{align}
        [Y_2, Y_3] = 0 
        \,,
        \qquad
        [Y_2, Y_1] = Y_2 + Y_3
        \,,
        \qquad
        [Y_3, Y_1] = Y_2
        \..
        \label{bia:biaIVAlgebra}
    \end{align}

    \begin{itemize}
        \visible<2->{
        \item {\bfseries Propriedades algébricas:}
        \begin{itemize}
            \item $\dim\ \frak{b}' = 2$;
            \item Não abeliana;
            \begin{itemize}
                \item Álgebra derivada $\frak{b}'$ é abeliana.
            \end{itemize}
            \item Centro: $Z(\frak{b}) = 0$;
            \item Não nilpotente;
            \item Solúvel;
            \item Radical: $R(\frak{b}) = \frak{b}$;
            \item Thurnston type: none.
        \end{itemize}
        }

        \visible<3->{
        \item {\bfseries Propriedades físicas:}
        \begin{itemize}
            \item Métrica
                \begin{align*}
                    ds^2 = d\tau^2 - A^2(\tau) dx^2 - B^2(\tau) e^{2x} dy^2
                                                    - C^2(\tau) e^{2x}(xdy + dz)^2
                                                    \,;
                \end{align*}

            \item Tríades:
            \begin{align*}
            [\tetD{a}{i}] = \left(\begin{matrix}
                    0 & 0 & 1   \\
                    1 & 0 & x^2 + x^3 \\
                    0 & 1 & x^3
               \end{matrix}\right)
               \qquad
               \,,
            [\tetU{a}{i}] = \left(\begin{matrix}
                    -x^2-x^3 & 1 & 0 \\
                    -x^3     & 1 & 0 \\
                    1        & 0 & 0
               \end{matrix}\right)
               \..
            \end{align*}
        \end{itemize}
        }
    \end{itemize}
\end{fframe}
}}}

\renewcommand\subsecname{Classificação -- $\frak{b}_{V}$}
{{{
\begin{fframe}
    Álgebra:
    \begin{align}
        [Y_2, Y_3] = Y_2
        \,,
        \qquad
        [Y_2, Y_3] = 0
        \,,
        \qquad
        [Y_3, Y_1] = Y_3
        \..
        \label{bia:biaVAlgebra}
    \end{align}

    \begin{itemize}
        \visible<2->{
        \item {\bfseries Propriedades algébricas:}
        \begin{itemize}
            \item $\dim\ \frak{b}' = 2$;
            \item Não abeliana;
            \begin{itemize}
                \item Álgebra derivada $\frak{b}'$ é abeliana;
            \end{itemize}
            \item Centro: $Z(\frak{b}) = 0$;
            \item Não nilpotente;
            \item Solúvel;
            \item Radical: $R(\frak{g}) = \frak{g}$;
            \item Thurnston type: $\mathds{H}^3$.
        \end{itemize}
        }

        \visible<3->{
        \item {\bfseries Propriedades físicas:}
        \begin{itemize}
            \item Métrica
                \begin{align*}
                    ds^2 = d\tau^2 - A^2(\tau) dx^2 - B^2(\tau) e^{-2ax} dy^2 
                                                    - C^2(\tau) e^{-2ax} dz^2
                                                    \ \,, \quad a = \text{const} \,;
                \end{align*}

            \item Corresponde ao setor aberto de \FLRW\ ($k = -1$);
            \item Admite soluções ``tilted'';
\iftoggle{tikz}{
            \begin{tikzpicture} [remember picture,overlay]
                \node[expblock] at (5,2) {Postulado de Weyl $u^\mu = (1, \bs{0})$};
            \end{tikzpicture}
}{}

            \item Tríades:
            \begin{align*}
            [\tetD{a}{i}] = \left(\begin{matrix}
                    0 & 0 & 1   \\
                    1 & 0 & x^2 \\
                    0 & 1 & x^3
               \end{matrix}\right)
               \,,
               \qquad
            [\tetU{a}{i}] = \left(\begin{matrix}
                    -x^2     & 1 & 0 \\
                    -x^3     & 0 & 1 \\
                    1        & 0 & 0
               \end{matrix}\right)
               \..
            \end{align*}

        \end{itemize}
        }
    \end{itemize}
\end{fframe}
}}}

\renewcommand\subsecname{Classificação -- $\frak{b}_{VI_0}$}
{{{
\begin{fframe}
    Álgebra:
    \begin{align}
        [X_1, X_2] = 0
        \,,
        \qquad
        [X_2, X_3] = X_1
        \,,
        \qquad
        [X_1, X_3] = X_2
        \..
        \label{bia:biaVI0Algebra}
    \end{align}

    \begin{itemize}
        \visible<2->{
        \item {\bfseries Propriedades algébricas:}
        \begin{itemize}
            \item $\dim\ \frak{b}' = 2$;
            \item Não abeliana;
            \begin{itemize}
                \item Álgebra derivada $\frak{b}'$ é abeliana.
            \end{itemize}
            \item Centro: $Z(\frak{b}) = 0$;
            \item Não nilpotente;
            \item Solúvel;
            \item Radical: $R(\frak{b}) = \frak{b}$;
            \item Thurnston type: $Sol$.
        \end{itemize}
        }

        \visible<3->{
        \item {\bfseries Propriedades físicas:}
        \begin{itemize}
            \item Métrica
                \begin{align*}
                    ds^2 = d\tau^2 - A^2(\tau) dx^2 - B^2(\tau) e^{-2ax} dy^2
                                                    - C^2(\tau) e^{2ax} dz^2
                                                    \ \,, \quad a = \text{const}
                                                    \,;
                \end{align*}

            \item Tríades:
            \begin{align*}
            [\tetD{a}{i}] = \left(\begin{matrix}
                    0 & 0 & 1   \\
                    1 & 0 & x^2 \\
                    0 & 1 & 0
               \end{matrix}\right)
               \,,
               \qquad
            [\tetU{a}{i}] = \left(\begin{matrix}
                    -x^2     & 1 & 0 \\
                    0        & 0 & 1 \\
                    1        & 0 & 0
               \end{matrix}\right)
               \..
            \end{align*}

        \end{itemize}
        }
    \end{itemize}
\end{fframe}
}}}

\renewcommand\subsecname{Classificação -- $\frak{b}_{VI_a}$}
{{{
\begin{fframe}
    Álgebra:
    \begin{align}
        [Y_2, Y_1] = aY_3
        \,,
        \qquad
        [Y_2, Y_3] = 0
        \,,
        \qquad
        [Y_3, Y_1] = Y_2 + Y_3
        \..
         \label{bia:biaVIAlgebra}
    \end{align}

    \begin{itemize}
        \visible<2->{
        \item {\bfseries Propriedades algébricas:}
        \begin{itemize}
            \item $\dim\ \frak{b}' = 2$;
            \item Não abeliana;
            \begin{itemize}
                \item Álgebra derivada $\frak{b}'$ é abeliana.
            \end{itemize}
            \item Centro: $Z(\frak{b}) = 0$;
            \item Não nilpotente;
            \item Solúvel;
            \item Radical: $R(\frak{b}) = \frak{b}$;
            \item Thurnston type: $\mathds{H}^2 \times \mathds{E}$ (apenas para $a = -1$).
        \end{itemize}
        }
        
        \visible<3->{
        \item {\bfseries Propriedades físicas:}
        \begin{itemize}
            \item Métrica
                \begin{align*}
                    ds^2 = d\tau^2 - A^2(\tau) dx^2 - B^2(\tau) e^{2x} dy^2 
                                                 - C^2(\tau) e^{2ax} dz^2
                                                 \ \,, \quad a \text{from the algebra}
                                                 \,,
                \end{align*}

            \item Tríades:
            \begin{align*}
            [\tetD{a}{i}] = \left(\begin{matrix}
                    0 & 0 & 1   \\
                    1 & 0 & x^2 \\
                    0 & 1 & ax^3
               \end{matrix}\right)
               \,,
               \qquad
            [\tetU{a}{i}] = \left(\begin{matrix}
                    -x^2     & 1 & 0 \\
                    -ax^3     & 0 & 1 \\
                    1        & 0 & 0
               \end{matrix}\right)
               \..
            \end{align*}

        \end{itemize}
        }
    \end{itemize}
\end{fframe}
}}}

\renewcommand\subsecname{Classificação -- $\frak{b}_{VII_0}$}
{{{ 
\begin{fframe}
    Álgebra:
    \begin{align}
        [X_1, X_2] = 0
        \,,
        \qquad
        [X_2, X_3] = X_1
        \,,
        \qquad
        [X_1, X_3] = -X_2
        \..
        \label{bia:biaVII0Algebra}
    \end{align}

    \vspace{-.2cm}
    \begin{itemize}
        \visible<2->{
        \item {\bfseries Propriedades algébricas:}
        \begin{itemize}
            \item $\dim\ \frak{b}' = 2$;
            \item Não abeliana;
            \begin{itemize}
                \item The derived algebra $\frak{b}'$ is abelian.
            \end{itemize}
            \item Centro: $Z(\frak{b}) = 0$;
            \item Não nilpotente;
            \item Solúvel;
            \item Radical: $R(\frak{b}) = \frak{b}$;
            \item Thurnston type: $\mathds{E}^3$.
        \end{itemize}
        }

        \visible<3->{
        \item {\bfseries Propriedades físicas:}
        \begin{itemize}
            \item Métrica
                \begin{align*}
                    ds^2 &= d\tau^2 - A^2(\tau) dx^2 
                                  - (B^2(\tau) \cos^2x - D^2(\tau)\sin^2x)dy^2
                                                                                \nonumber \\
                                                                                &\quad
                                  - 2\cos x\sin x (B^2(\tau) - D^2(\tau)) dy dz
                                  - (B^2(\tau)\sin^2x + D^2(\tau)\cos^2x)dz^2
                                  \,;
                \end{align*}

            \item Pode ser isotrópico;
            \item Corresponde ao setor plano de \FLRW\ ($k = 0$);
            \item Admite soluções ``tilted'';

            \item Tríades:
            \begin{align*}
            [\tetD{a}{i}] = \left(\begin{matrix}
                    0 & 0 & 1    \\
                    1 & 0 & -x^3 \\
                    0 & 1 & x^2
               \end{matrix}\right)
               \,,
               \qquad
            [\tetU{a}{i}] = \left(\begin{matrix}
                    x^3     & 1 & 0 \\
                    -x^2    & 0 & 1 \\
                    1       & 0 & 0
               \end{matrix}\right)
               \..
            \end{align*}


        \end{itemize}
        }
    \end{itemize}
\end{fframe}
}}}

\renewcommand\subsecname{Classificação -- $\frak{b}_{VII_a}$}
{{{
\begin{fframe}
    Álgebra:
    \begin{align}
        [Y_2, Y_1] = aY_3
        \,,
        \qquad
        [Y_2, Y_3] = 0
        \,,
        \qquad
        [Y_3, Y_1] = Y_2 + Y_3
        \..
        \label{bia:biaVIIAlgebra}
    \end{align}

    \begin{itemize}
        \visible<2->{
        \item {\bfseries Propriedades algébricas:}
        \begin{itemize}
            \item $\dim\ \frak{b}' = 2$;
            \item Não abeliana;
            \begin{itemize}
                \item Álgebra derivada $\frak{g}'$ é abeliana.
            \end{itemize}
            \item Centro: $Z(\frak{b}) = 0$;
            \item Não nilpotente;
            \item Solúvel;
            \item Radical: $R(\frak{g}) = \frak{g}$;
            \item Thurnston type: $\mathds{H}^3$.
        \end{itemize}
        }

        \visible<3->{
        \item {\bfseries Propriedades físicas:}
        \begin{itemize}
            \item Métrica
                \begin{align*}
                    ds^2 &= d\tau^2 - A^2(t)(\cos \psi dx - \sin \psi dy)^2
                                   - B^2(t) (\sin \psi dx + \cos \psi dy)^2
                                   - C^2(t) dz^2
                \end{align*}

\iftoggle{tikz}{
            \begin{tikzpicture} [remember picture,overlay]
                \node[expblock] at (7,0) {$\psi=\psi(\tau)$};
            \end{tikzpicture}
}{}
                
            \item Pode ser isotrópico;
            \item Corresponde ao setor aberto de \FLRW\ ($k = -1$);
            \item Admite soluções ``tilted'';

            \item Tríades:
            \begin{align*}
            [\tetD{a}{i}] = \left(\begin{matrix}
                    0 & 0 & 1    \\
                    1 & 0 & -x^3 \\
                    0 & 1 & x^2 + ax^3
               \end{matrix}\right)
               \,,
               \qquad
            [\tetU{a}{i}] = \left(\begin{matrix}
                    x^3            & 1 & 0 \\
                    -x^2 - ax^3    & 0 & 1 \\
                    1              & 0 & 0
               \end{matrix}\right)
               \..
            \end{align*}

        \end{itemize}
        }
    \end{itemize}
\end{fframe}
}}}

\renewcommand\subsecname{Classificação -- $\frak{b}_{VIII}$}
{{{
\begin{fframe}
    Álgebra:
   \begin{align}
        [X_1, X_2] = -X_3
        \,,
        \qquad
        [X_2, X_3] = X_1
        \,,
        \qquad
        [X_3, X_1] = X_2
        \..
        \label{bia:biaVIIIAlgebra}
    \end{align}

    \begin{itemize}
        \visible<2->{
        \item {\bfseries Propriedades algébricas:}
        \begin{itemize}
            \item $\dim\ \frak{b}' = 3$;
            \item $\frak{b}' = \frak{b}$;
            \item $\frak{b}$ é simples;
            \item Centro: $Z(\frak{b}) = 0$;
            \item Não nilpotente;
            \item Não solúvel;
            \item Radical: $R(\frak{b}) = 0$;
            \item Isomorfa a $\frak{gl}(V)$, $V$: espaço vetorial;
            \item Thurnston type: $\widetilde{SL(2,\mathds{R})}$.
        \end{itemize}
        }

        \visible<3->{
        \item {\bfseries Propriedades físicas:}
        \begin{itemize}
            \item Métrica
                \begin{align*}
                    ds^2 = d\tau^2 - S^2(\tau) dx^2
                                   - R^2(\tau) ( dy^2 + \sinh^2y dz^2 )
                                   - S^2(\tau) \cosh y ( 2dx + \cosh y dz ) dz
                                   \,,
                \end{align*}

            \item Pode ser entendido como um modelo isotrópico com ``reflexão axial'';
            \item Soluções oscilatórias perto da singularidade fundamental (Mixmaster);

            \item Tríades:
            \begin{align*}
            [\tetD{a}{i}] = \left(\begin{matrix}
                    e^{-x^3}          & 0 & 0       \\
                    -(x^2)^2 e^{-x^3} & 0 & e^{x^3} \\
                    -2x^2 e^{-x^3}    & 1 & 0
               \end{matrix}\right)
               \,,
               \qquad
            [\tetU{a}{i}] = \left(\begin{matrix}
                    e^{x^3}          & 0        & 0 \\
                    2x^2             & 0        & 1 \\
                    (x^2)^2 e^{-x^3} & e^{-x^3} & 0
               \end{matrix}\right)
               \..
            \end{align*}

        \end{itemize}
        }
    \end{itemize}
\end{fframe}
}}}

\fi
\renewcommand\subsecname{Classificação -- $\frak{b}_{IX}$}
{{{
\begin{fframe}
    Álgebra:
    \begin{align}
        [X_1, X_2]  = X_3
        \,,
        \quad
        [X_2, X_3] &= X_1
        \,,
        \quad
        [X_3, X_1]  = X_2
        \implies
        [X_i, X_j] = \epsilon_{ijk} X_k
        \..
        \label{bia:biaIXAlgebra}
    \end{align}

    \vspace{-.5cm}
    \begin{itemize}
        \visible<2->{
        \item {\bfseries Propriedades algébricas:}
        \begin{itemize}
            \item $\dim\ \frak{b}' = 3$;
            \item $\frak{b}' = \frak{b}$;
            \item $\frak{b}$ é simples;
            \item Centro: $Z(\frak{g}) = 0$;
            \item Não solúvel;
            \item Não nilpotente;
            \item Radical: $R(\frak{g}) = 0$;
            \item Isomorfa a $\frak{so}(3)$;
            \item Thurnston type: $\mathds{S}^3$.
        \end{itemize}
        }
        
        \visible<3->{
        \item {\bfseries Propriedades físicas:}
        \begin{itemize}
            \item Métrica
                \begin{align*}
                    ds^2 = d\tau^2 - S^2(\tau) dx^2 
                                   - R^2(\tau) ( dy^2 + \sin^2y dz^2 )
                                   + S^2(\tau) \cos y( 2dx - \cos y dz )dz
                                   \,,
                \end{align*}

            \item Corresponde ao setor fechado de \FLRW\ ($k=+1$);
            \item Isotrópico $\implies$ simetria máxima;
            \item Grupo de movimentos $SO(3)$;
            \item Soluções oscilatórias perto da singularidade fundamental (Mixmaster);
            \item Tipo mais interessante;
        \end{itemize}
        }
    \end{itemize}
\end{fframe}

\begin{fframe}
        \begin{itemize}
            \item[]
            \begin{itemize}
            \item Tríades:
            \begin{align*}
            [\tetD{a}{i}] = \left(\begin{matrix}
                    0 & \cos x^2                  & -\sin x^2         \\
                    1 & -\cot x^1 \sin x^2        & \cot x^1 \cos x^2 \\
                    0 & \frac{\sin x^2}{\sin x^1} & \frac{\cos x^2}{\sin x^1}
               \end{matrix}\right)
               \,,
            \end{align*}
            \begin{align*}
            [\tetU{a}{i}] = \left(\begin{matrix}
                    0         & 1 & \cos x^1          \\
                    \cos x^2  & 0 & \sin x^1 \sin x^2 \\
                    -\sin x^2 & 0 & \sin x^1 \cos x^2
               \end{matrix}\right)
               \..
            \end{align*}


        \end{itemize}
    \end{itemize}
\end{fframe}
}}}

\renewcommand\subsecname{Classificação de Bianchi}
\begin{fframe}
    Revisitando a tabela da classificação
 
    \fontsize{6.5pt}{6.5pt}\selectfont
    \begin{table}[h!]
        \centering
        \begin{tabular}{clcccccc}
        \hline
        Class & Type    &   $a$     &   $n^{(1)}$   &   $n^{(2)}$   &   $n^{(3)}$
              &  Thurston    &  Note \\ \hline
        \mr{6}{A}   & I & 0   & 0 &  0 &  0 
                        & $\mathds{E}^3$ & \FLRW\ $k=0$, Petrov O \\
        &II             & 0   & 1 &  0 &  0 
                        & $Nil$ & Heisenberg algebra \\
        &VII${}_0$      & 0   & 1 &  1 &  0 
                        & $\mathds{E}^3$ & \FLRW\ $k=0$, Petrov O \\
        &VI${}_0$       & 0   & 1 & -1 &  0 
                        & $Sol$ & $--$ \\
        &IX             & 0   & 1 &  1 &  1 
                        & $\mathds{S}^3$ & \FLRW\ $k=+1$, Petrov O, Mixmaster solution \\
        &VIII           & 0   & 1 &  1 & -1 
                        & $\widetilde{SL2\mathds{R}}$ & Mixmaster solution \\
        \hline
        \mr{4}{B}   & V & 1   & 0 &  0 &  0 & $\mathds{H}^3$ & \FLRW\ $k=-1$, Petrov O \\
        &IV             & 1   & 0 &  0 &  1 
                        & $--$ & $--$ \\
        &VII${}_a$      & $a$ & 0 &  1 &  1 
                        & $\mathds{H}^3$ & \FLRW\ $k=-1$, Petrov O \\
        &III ($a=1$) \ \ \rdelim\}{2}{0pt}[]    \
                        &\mr{2}{$a$}& \mr{2}{0}     &\mr{2}{1}      &\mr{2}{-1}
                        & $\mathds{H}^2\times\mathds{E}$ & Petrov D \\
        &VI${}_a$ ($a\ne 1$) \
                        & & & & 
                        & $\mathds{H}^2\times\mathds{E}$ & Only for $a=-1$ \\
        \hline
        \end{tabular}
        \caption{Classificação de Bianchi -- Revisitada}
                                                            \label{tab:hom:bianchiRevisited}
    \end{table}


\end{fframe}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -- ---------------------- -- %%%%%%%%%%%
